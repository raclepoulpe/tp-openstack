# TP OVHcloud Public Cloud

# Prérequis

## Compte Public Cloud OVHcloud (Cloud Provider)

Créer votre compte [OVHcloud](https://www.ovh.com/) et créez un projet [Public Cloud](https://www.ovh.com/manager/#/public-cloud/pci/projects).

## OVHcloud API keys

Générez vos accès à l'API OVHcloud.

[API OVHcloud](https://api.ovh.com/)

[Creation token](https://www.ovh.com/auth/api/createToken)

## Compte Gitlab (Source Repository)

Créez un compte [Gitlab](https://gitlab.com/).

## Compte Gitpod (Online IDE)

Créez un compte [Gitpod](https://gitpod.io/).

## Variables Gitpod

Dans le [Dashboard Gitpod](https://gitpod.io/variables), créez les variables suivantes avec leurs contenus respectifs:

| Variable Name  | base64 encoded | Content
|---|---|---
| OS_OPENRC_B64 | yes | openrc file content 
| OVH_API_AK_b64  | yes | OVHcloud API Application Key value
| OVH_API_AS_b64 | yes | OVHcloud API Application Secret Value
| OVH_API_CK_b64 | yes | OVHcloud API Consumer Key
